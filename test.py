# encoding=utf8
import numpy as np
from torch.autograd import Variable
import os
import sys
from gensim.models.word2vec import Word2Vec
import torch
from models import FlatCnnLayer, TemporalConvNet
from numpy import linalg as LA

USE_CUDA = False
if 'USE_CUDA' in os.environ:
    USE_CUDA = (os.environ['USE_CUDA'].lower() == 'true') and torch.cuda.is_available()

print 'USE CUDA: {}'.format(USE_CUDA)

TITLE_LIMIT = 25
TAG_LIMIT = 10
WORD_EMBEDDING_SIZE = 300
FILTER_SIZES = [3, 4, 5]
UNK = "UNK"


def get_word_embedding(word_vectors, token):
    if token not in word_vectors.vocab:
        token = UNK
    return word_vectors[token]


def build_dataset_cnn(items, word_vectors):
    X = np.zeros([len(items), TITLE_LIMIT + TAG_LIMIT, WORD_EMBEDDING_SIZE])
    list_uniq_cate = []
    cates = np.zeros(len(items), dtype=np.dtype(int))

    for i, item in enumerate(items.values()):
        if item['category'] not in list_uniq_cate:
            list_uniq_cate.append(item['category'])
        cates[i] = item['category']

        for j, token in enumerate(item['title'][:TITLE_LIMIT]):
            X[i, j] = get_word_embedding(word_vectors, token)
        for j, token in enumerate(item['tag'][:TAG_LIMIT]):
            X[i, j + TITLE_LIMIT] = get_word_embedding(word_vectors, token)

    return np.expand_dims(X, 1), cates, len(list_uniq_cate)


def build_dataset_dynamic_cnn(items, word_vectors):
    X = []
    list_uniq_cate = []
    cates = np.zeros(len(items), dtype=np.dtype(int))

    for i, item in enumerate(items.values()):
        if item['category'] not in list_uniq_cate:
            list_uniq_cate.append(item['category'])
        cates[i] = item['category']

        x = []
        for token in item['title']:
            x.append(get_word_embedding(word_vectors, token))
        for tag in item['tag']:
            x.append(get_word_embedding(word_vectors, tag))
        X.append(np.array(x))

    return X, cates, len(list_uniq_cate)


def num_conv_channel(out, n_layer=3):
    return [1024] * (n_layer - 1) + [out]


def load_items(domain):
    items = {}
    first_line = True

    with open('assets/' + domain + '/items.txt', mode='r') as f:
        for line in f:
            if first_line:
                first_line = False
                continue
            parts = line.strip().split("||")
            newsId = long(parts[0])
            category = int(parts[1])
            title = parts[2].split()
            content = parts[3].split()
            publishDate = long(parts[4])
            tag = parts[5].split()
            items[newsId] = {
                'newsId': newsId,
                'category': category,
                'title': title,
                'content': content,
                'publishDate': publishDate,
                'tag': tag
            }
    return items


if __name__ == '__main__':
    domain = sys.argv[1]
    model_name = sys.argv[2]
    out_channels = int(sys.argv[3])
    items = load_items(domain)

    dictionary = Word2Vec.load('assets/dictionary')
    word_vectors = dictionary.wv
    print 'Finish loading Word2Vec model! Size: %d' % len(word_vectors.vocab)
    del dictionary

    embedding_items = {}
    count = 0

    if model_name == 'cnn':
        net = FlatCnnLayer(embedding_size=WORD_EMBEDDING_SIZE,
                           sequence_length=TITLE_LIMIT + TAG_LIMIT,
                           filter_sizes=FILTER_SIZES,
                           out_channels=out_channels)
        if USE_CUDA:
            net = net.cuda()
            net.load_state_dict(torch.load("dist/model_{}_cnn_{}".format(domain, out_channels)))
        else:
            net.load_state_dict(torch.load("dist/model_{}_cnn_{}".format(domain, out_channels),
                                           map_location=lambda storage, loc: storage))
        net.eval()

        for newsId, item in list(items.items()):
            count += 1
            print('\r\033[K\rCalculating item embedding {}/{}'.format(count, len(items))),
            sys.stdout.flush()
            inputs, _, _ = build_dataset_cnn({newsId: item}, word_vectors)
            inputs = torch.from_numpy(inputs).float()
            if USE_CUDA:
                inputs = inputs.cuda()
            inputs = Variable(inputs)
            doc_embedding = net(inputs)[0].cpu().detach().numpy()
            doc_embedding = doc_embedding / LA.norm(doc_embedding)
            embedding_items[newsId] = doc_embedding

    if model_name == 'tcn':
        net = TemporalConvNet(WORD_EMBEDDING_SIZE, num_conv_channel(out_channels, 5))
        if USE_CUDA:
            net = net.cuda()
            net.load_state_dict(torch.load("dist/model_{}_tcn_{}".format(domain, out_channels)))
        else:
            net.load_state_dict(torch.load("dist/model_{}_tcn_{}".format(domain, out_channels),
                                           map_location=lambda storage, loc: storage))
        net.eval()

        for newsId, item in list(items.items()):
            count += 1
            print('\r\033[K\rCalculating item embedding {}/{}'.format(count, len(items))),
            sys.stdout.flush()
            inputs, _, _ = build_dataset_dynamic_cnn({newsId: item}, word_vectors)
            inputs = torch.from_numpy(np.array(inputs)).float()
            # convert to cuda tensors if cuda flag is true
            if USE_CUDA:
                inputs = inputs.cuda()
            # wrap them in Variable
            inputs = Variable(inputs)
            doc_embedding = net(inputs.transpose(1, 2)).transpose(1, 2)[0, -1].cpu().detach().numpy()
            doc_embedding = doc_embedding / LA.norm(doc_embedding)
            embedding_items[newsId] = doc_embedding


    def item_sim(id1, id2):
        content_sim = np.dot(embedding_items[id1], embedding_items[id2])
        tag_sim = len(list(set(items[id1]['tag']).intersection(items[id2]['tag'])))
        cate_sim = 1 if items[id1]['category'] == items[id2]['category'] else 0

        return np.asscalar(content_sim) + 0.2 * tag_sim + 0.1 * cate_sim


    def custom_comparator(id1, id2, c_item_id):
        score = item_sim(c_item_id, id2) - item_sim(c_item_id, id1)
        if score > 0:
            return 1
        if score == 0:
            return 0
        return -1


    while True:
        print  # Example: 20180315093511322, 20180525153635453, 20180413112011272, 20180409083732063
        item_id = raw_input("\nNhập vào ID cua bài viết: ")
        if item_id.strip() == "":
            break
        item_id = long(item_id)
        if item_id not in items:
            print("ID không tồn tại")
            continue
        print("Bài đang xét: " + " ".join(items[item_id]['title']))
        candidate_items = items.copy()
        candidate_items.pop(item_id)

        sorted_ids = sorted(candidate_items.keys(), cmp=lambda id1, id2: custom_comparator(id1, id2, item_id))
        print("Danh sách top 10 bài liên quan được gợi ý:")

        for i in range(10):
            print("{}. {}".format(i + 1, " ".join(items[sorted_ids[i]]['title'])))
