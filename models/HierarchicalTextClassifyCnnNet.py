import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim
from FlatCnnLayer import FlatCnnLayer
from utils import TreeTools, get_learning_rate
import multiprocessing
import numpy as np
import os
import sys
from math import log10, floor

batch_size = 128
n_epochs = 160
log_interval = 1
learning_rate = 0.005
iter_check_w = 10
display_step = 5
N_WORKERS = max(1, multiprocessing.cpu_count() - 1)

USE_CUDA = False
if 'USE_CUDA' in os.environ:
    USE_CUDA = (os.environ['USE_CUDA'].lower() == 'true') and torch.cuda.is_available()


class HierarchicalTextClassifyCnnNet(nn.Module):
    def __init__(self, embedding_size, sequence_length, tree, filter_sizes=[3, 4, 5], out_channels=128):
        super(HierarchicalTextClassifyCnnNet, self).__init__()

        self._tree_tools = TreeTools()
        self.tree = tree
        # create a weight matrix and bias vector for each node in the tree
        self.classifier = nn.ModuleList([nn.Linear(out_channels * len(filter_sizes), len(subtree[1])) for subtree in
                                         self._tree_tools.get_subtrees(tree)])

        self.value_to_path_and_nodes_dict = {}
        for path, value in self._tree_tools.get_paths(tree):
            nodes = self._tree_tools.get_nodes(tree, path)
            self.value_to_path_and_nodes_dict[value] = path, nodes

        self.flat_layer = FlatCnnLayer(embedding_size, sequence_length, filter_sizes=filter_sizes,
                                       out_channels=out_channels)

        self.features = nn.Sequential(self.flat_layer)
        for m in self.modules():
            if isinstance(m, nn.Linear):
                init.xavier_uniform(m.weight, gain=np.sqrt(2.0))
                init.constant(m.bias, 0.1)

    def forward(self, inputs, targets):
        features = self.features(inputs)
        predicts = map(self._get_predicts, features, targets)
        losses = map(self._get_loss, predicts, targets)
        return losses, predicts

    def _get_loss(self, predicts, label):
        path, _ = self.value_to_path_and_nodes_dict[int(label.item())]
        criterion = nn.CrossEntropyLoss()
        if USE_CUDA:
            criterion = criterion.cuda()

        def f(predict, p):
            p = torch.LongTensor([p])
            # convert to cuda tensors if cuda flag is true
            if USE_CUDA:
                p = p.cuda()
            p = Variable(p)
            return criterion(predict.unsqueeze(0), p).unsqueeze(0)

        loss = map(f, predicts, path)
        return torch.sum(torch.cat(loss, dim=0)).unsqueeze(0)

    def _get_predicts(self, feature, label):
        _, nodes = self.value_to_path_and_nodes_dict[int(label.item())]

        predicts = map(lambda n: self.classifier[n](feature), nodes)
        return predicts


def fit(model, data, save_path, report_path):
    if USE_CUDA:
        model = model.cuda()

    optimizer = optim.Adam([
        {'params': model.features.parameters()},
        {'params': model.classifier.parameters(), 'weight_decay': 0.1}
    ], lr=learning_rate)
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.3, patience=5, min_lr=1e-3,
                                                     verbose=True)

    x_train, x_test = torch.from_numpy(data['X_train']).float(), torch.from_numpy(data['X_test']).float()
    y_train, y_test = torch.from_numpy(data['Y_train']).int(), torch.from_numpy(data['Y_test']).int()

    x_valid, y_valid = x_train[:(x_train.size()[0] // 6)], y_train[:(y_train.size()[0] // 6)]

    try:
        for epoch in range(1, n_epochs + 1):  # loop over the dataset multiple times

            # shuffle train set
            perm = torch.randperm(x_train.size()[0])
            x_train, y_train = x_train[perm], y_train[perm]

            # run train over train set then evaluate model over validation set
            train_loss = _train(model, x_train, y_train, optimizer, epoch)
            val_loss, val_accuracy = _evaluate(model, x_valid, y_valid)
            with open(report_path, mode='a') as f:
                f.write('{},{:.5f},{:.5f}\n'.format(epoch, train_loss, val_loss))

            # print statistics
            if epoch % display_step == 0 or epoch == 1:
                print('\r\033[K\r[{:3d}] loss: {:.5f} - learning rate: {} - valid accuracy: {:.2f}%'
                      .format(epoch, train_loss, get_learning_rate(optimizer)[0], val_accuracy))

            # Save the model if the validation loss is the best we've seen so far.
            if not scheduler.best or scheduler.is_better(val_loss, scheduler.best):
                with open(save_path, 'wb') as f:
                    torch.save(model.flat_layer.state_dict(), f)
            scheduler.step(val_loss)

    except KeyboardInterrupt:
        print('-' * 89)
        print('Exiting from training early')

    print('Finished Training. Saved report at {}\n'.format(report_path))

    if USE_CUDA:
        model.flat_layer.load_state_dict(torch.load(save_path))
    else:
        model.flat_layer.load_state_dict(torch.load(save_path, map_location=lambda storage, loc: storage))

    _, test_accuracy = _evaluate(model, x_test, y_test)
    print ('Accuracy of the network {:.2f}%'.format(test_accuracy))


def _check_correct_predicts(model, predicts, label):
    path, _ = model.value_to_path_and_nodes_dict[int(label.item())]
    for predict, p in zip(predicts, path):
        if np.argmax(predict.data) != p:
            return 0
    return 1


def _train(model, x, y, optimizer, epoch):
    model.train()
    acc_loss = 0.0

    n_batches = len(x) // batch_size

    for batch_idx, i in enumerate(range(n_batches), 1):
        start = i * batch_size
        end = start + batch_size
        if i == n_batches - 1:
            end += len(x) % batch_size
        inputs, labels = x[start:end], y[start:end]
        # convert to cuda tensors if cuda flag is true
        if USE_CUDA:
            inputs, labels = inputs.cuda(), labels.cuda()
        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        losses, _ = model(inputs, labels)
        loss = torch.mean(torch.cat(losses, dim=0))
        acc_loss += loss.item()

        if batch_idx % log_interval == 0:
            offset = int(floor(log10(n_batches)) - floor(log10(batch_idx)))
            print('\r\033[K\rTrain Epoch: {} [{}{} / {} ({:.0f}%)]   Learning Rate: {}   Loss: {:.6f}'
                  .format(epoch, batch_idx, ' ' * offset, n_batches, 100. * batch_idx / n_batches,
                          get_learning_rate(optimizer)[0], loss.item())),
            sys.stdout.flush()

        loss.backward()
        optimizer.step()

    return acc_loss / n_batches


def _evaluate(model, x, y):
    model.eval()

    nb_test_corrects, nb_test_samples = 0, 0
    acc_loss = 0.0

    n_batches = len(x) // batch_size
    for i in range(n_batches):
        start = i * batch_size
        end = start + batch_size
        if i == n_batches - 1:
            end += len(x) % batch_size
        inputs, labels = x[start:end], y[start:end]
        # convert to cuda tensors if cuda flag is true
        if USE_CUDA:
            inputs, labels = inputs.cuda(), labels.cuda()
        # wrap them in Variable
        inputs, labels = Variable(inputs), Variable(labels)

        # forward + backward + optimize
        losses, predicts = model(inputs, labels)
        loss = torch.mean(torch.cat(losses, dim=0))
        acc_loss += loss.item()

        nb_test_samples += labels.size(0)
        for predicted, label in zip(predicts, labels):
            nb_test_corrects += _check_correct_predicts(model, predicted, label)
    return acc_loss / n_batches, 100. * nb_test_corrects / nb_test_samples
