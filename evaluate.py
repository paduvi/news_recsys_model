# encoding=utf8
import numpy as np
from torch.autograd import Variable
import os
import sys
from gensim.models.word2vec import Word2Vec, FAST_VERSION
import torch
from models import FlatCnnLayer, TemporalConvNet
from numpy import linalg as LA

print 'Current gensim FAST_VERSION: %d' % FAST_VERSION
assert FAST_VERSION > -1

USE_CUDA = False
if 'USE_CUDA' in os.environ:
    USE_CUDA = (os.environ['USE_CUDA'].lower() == 'true') and torch.cuda.is_available()

print 'USE CUDA: {}'.format(USE_CUDA)

TITLE_LIMIT = 25
TAG_LIMIT = 10
WORD_EMBEDDING_SIZE = 300
FILTER_SIZES = [3, 4, 5]
UNK = "UNK"


def get_word_embedding(word_vectors, token):
    if token not in word_vectors.vocab:
        token = UNK
    return word_vectors[token]


def load_items(domain):
    items = {}
    first_line = True

    with open('assets/' + domain + '/items.txt', mode='r') as f:
        for line in f:
            if first_line:
                first_line = False
                continue
            parts = line.strip().split("||")
            newsId = long(parts[0])
            category = int(parts[1])
            title = parts[2].split()
            content = parts[3].split()
            publishDate = long(parts[4])
            tag = parts[5].split()
            items[newsId] = {
                'newsId': newsId,
                'category': category,
                'title': title,
                'content': content,
                'publishDate': publishDate,
                'tag': tag
            }
    return items


def load_history(domain):
    users = {}
    with open('assets/' + domain + '/history.txt', mode='r') as f:
        for line in f:
            try:
                parts = line.strip().split(";", 1)
                guid = parts[0]
                logs = parts[1]
                users[guid] = [long(item) for item in logs.split()]
            except IndexError, e:
                pass
    return users


def build_dataset_cnn(items, word_vectors):
    X = np.zeros([len(items), TITLE_LIMIT + TAG_LIMIT, WORD_EMBEDDING_SIZE])
    list_uniq_cate = []
    cates = np.zeros(len(items), dtype=np.dtype(int))

    for i, item in enumerate(items.values()):
        if item['category'] not in list_uniq_cate:
            list_uniq_cate.append(item['category'])
        cates[i] = item['category']

        for j, token in enumerate(item['title'][:TITLE_LIMIT]):
            X[i, j] = get_word_embedding(word_vectors, token)
        for j, token in enumerate(item['tag'][:TAG_LIMIT]):
            X[i, j + TITLE_LIMIT] = get_word_embedding(word_vectors, token)

    return np.expand_dims(X, 1), cates, len(list_uniq_cate)


def build_dataset_dynamic_cnn(items, word_vectors):
    X = []
    list_uniq_cate = []
    cates = np.zeros(len(items), dtype=np.dtype(int))

    for i, item in enumerate(items.values()):
        if item['category'] not in list_uniq_cate:
            list_uniq_cate.append(item['category'])
        cates[i] = item['category']

        x = []
        for token in item['title']:
            x.append(get_word_embedding(word_vectors, token))
        for tag in item['tag']:
            x.append(get_word_embedding(word_vectors, tag))
        X.append(np.array(x))

    return X, cates, len(list_uniq_cate)


def num_conv_channel(out, n_layer=3):
    return [1024] * (n_layer - 1) + [out]


def get_tag_trunk(items, id1, id2):
    p = set(items[id1]['tag']) & set(items[id2]['tag'])
    return len(p)


def cosine_similarity(vector1, vector2):
    return np.inner(vector1, vector2) / (LA.norm(vector1) * LA.norm(vector2))


if __name__ == '__main__':
    k = 20
    domain = sys.argv[1]
    model_name = sys.argv[2]
    out_channels = int(sys.argv[3])

    dictionary = Word2Vec.load('assets/dictionary')
    word_vectors = dictionary.wv
    print 'Finish loading Word2Vec model! Size: %d' % len(word_vectors.vocab)
    del dictionary

    items = load_items(domain)
    print('Load items completed')
    users = load_history(domain)
    print('Load history completed')
    embedding_items = {}
    count = 0

    if model_name == 'cnn':
        net = FlatCnnLayer(embedding_size=WORD_EMBEDDING_SIZE,
                           sequence_length=TITLE_LIMIT + TAG_LIMIT,
                           filter_sizes=FILTER_SIZES,
                           out_channels=out_channels)
        if USE_CUDA:
            net = net.cuda()
            net.load_state_dict(torch.load("report/model_{}_cnn_{}".format(domain, out_channels)))
        else:
            net.load_state_dict(torch.load("report/model_{}_cnn_{}".format(domain, out_channels),
                                           map_location=lambda storage, loc: storage))
        net.eval()

        for newsId, item in list(items.items()):
            count += 1
            print('\r\033[K\rCalculating item embedding {}/{}'.format(count, len(items))),
            sys.stdout.flush()
            inputs, _, _ = build_dataset_cnn({newsId: item}, word_vectors)
            inputs = torch.from_numpy(inputs).float()
            if USE_CUDA:
                inputs = inputs.cuda()
            inputs = Variable(inputs)
            embedding_items[newsId] = net(inputs)[0].cpu().detach().numpy()

    if model_name == 'tcn':
        net = TemporalConvNet(WORD_EMBEDDING_SIZE, num_conv_channel(out_channels, 5))
        if USE_CUDA:
            net = net.cuda()
            net.load_state_dict(torch.load("report/model_{}_tcn_{}".format(domain, out_channels)))
        else:
            net.load_state_dict(torch.load("report/model_{}_tcn_{}".format(domain, out_channels),
                                           map_location=lambda storage, loc: storage))
        net.eval()

        for newsId, item in list(items.items()):
            count += 1
            print('\r\033[K\rCalculating item embedding {}/{}'.format(count, len(items))),
            sys.stdout.flush()
            inputs, _, _ = build_dataset_dynamic_cnn({newsId: item}, word_vectors)
            inputs = torch.from_numpy(np.array(inputs)).float()
            # convert to cuda tensors if cuda flag is true
            if USE_CUDA:
                inputs = inputs.cuda()
            # wrap them in Variable
            inputs = Variable(inputs)
            embedding_items[newsId] = net(inputs.transpose(1, 2)).transpose(1, 2)[0, -1].cpu().detach().numpy()

    mrr = 0
    tp = 0
    fp = 0
    fn = 0

    with open('assets/' + domain + '/test_data.txt', mode='r') as f:
        for line in f:
            parts = line.strip().split(";", 1)
            guid = parts[0]
            logs = parts[1]

            for log in logs.split():
                ts = long(log.split("||")[0])
                refered_item = long(log.split("||")[1])
                clicked_item = long(log.split("||")[2])

                if guid not in users:
                    users[guid] = []
                users[guid].append(refered_item)

                candidate_items = [newsId for newsId in items.keys()
                                   if newsId not in users[guid]
                                   and (ts - 3 * 24 * 3600) * 1000 <= items[newsId]['publishDate'] < ts * 1000]
                scores = {}
                for newsId in candidate_items:
                    score = cosine_similarity(embedding_items[newsId], embedding_items[refered_item])
                    score += 0.2 * get_tag_trunk(items, newsId, refered_item)
                    if items[newsId]['category'] == items[refered_item]['category']:
                        score += 0.1
                    scores[newsId] = score
                sorted_result = sorted(scores, key=scores.get, reverse=True)
                try:
                    rank = sorted_result.index(clicked_item) + 1
                    print('\r\033[K\r{} - Rank: {}'.format(tp + fn, rank)),
                    sys.stdout.flush()
                    mrr += 1 / rank
                    if rank <= k:
                        tp += 1
                    else:
                        fn += 1
                    fp += min(rank - 1, k)
                except ValueError, e:
                    continue

    print('Precision @{}: {:.4f}'.format(k, tp * 1. / (tp + fp)))
    print('Recall @{}: {:.4f}'.format(k, tp * 1. / (tp + fn)))
    print('MRR: {:.4f}'.format(mrr * 1. / (tp + fn)))
