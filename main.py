# encoding=utf8
from six.moves import cPickle as pickle
import numpy as np
import sys
import time
import os
import torch
from gensim.models.word2vec import Word2Vec, FAST_VERSION
from sklearn.model_selection import train_test_split
from models.HierarchicalTextClassifyCnnNet import HierarchicalTextClassifyCnnNet, fit as fit_cnn
from models.HierarchicalTextClassifyTcnNet import HierarchicalTextClassifyTcnNet, fit as fit_tcn

print 'Current gensim FAST_VERSION: %d' % FAST_VERSION
assert FAST_VERSION > -1

USE_CUDA = False
if 'USE_CUDA' in os.environ:
    USE_CUDA = (os.environ['USE_CUDA'].lower() == 'true') and torch.cuda.is_available()

print 'USE CUDA: {}'.format(USE_CUDA)

TITLE_LIMIT = 25
TAG_LIMIT = 10
WORD_EMBEDDING_SIZE = 300
FILTER_SIZES = [3, 4, 5]
UNK = "UNK"


def load_cate_tree(domain):
    with open('assets/' + domain + '/cate_tree.pickle', mode='rb') as f:
        return pickle.load(f)


def load_items(domain):
    items = {}
    first_line = True

    with open('assets/' + domain + '/items.txt', mode='r') as f:
        for line in f:
            if first_line:
                first_line = False
                continue
            parts = line.strip().split("||")
            newsId = long(parts[0])
            category = int(parts[1])
            title = parts[2].split()
            content = parts[3].split()
            publishDate = long(parts[4])
            tag = parts[5].split()
            items[newsId] = {
                'newsId': newsId,
                'category': category,
                'title': title,
                'content': content,
                'publishDate': publishDate,
                'tag': tag
            }
    return items


def get_word_embedding(word_vectors, token):
    if token not in word_vectors.vocab:
        token = UNK
    return word_vectors[token]


def build_dataset_cnn(items, word_vectors):
    X = np.zeros([len(items), TITLE_LIMIT + TAG_LIMIT, WORD_EMBEDDING_SIZE])
    list_uniq_cate = []
    cates = np.zeros(len(items), dtype=np.dtype(int))

    for i, item in enumerate(items.values()):
        if item['category'] not in list_uniq_cate:
            list_uniq_cate.append(item['category'])
        cates[i] = item['category']

        for j, token in enumerate(item['title'][:TITLE_LIMIT]):
            X[i, j] = get_word_embedding(word_vectors, token)
        for j, token in enumerate(item['tag'][:TAG_LIMIT]):
            X[i, j + TITLE_LIMIT] = get_word_embedding(word_vectors, token)

    return np.expand_dims(X, 1), cates, len(list_uniq_cate)


def build_dataset_dynamic_cnn(items, word_vectors):
    X = []
    list_uniq_cate = []
    cates = np.zeros(len(items), dtype=np.dtype(int))

    for i, item in enumerate(items.values()):
        if item['category'] not in list_uniq_cate:
            list_uniq_cate.append(item['category'])
        cates[i] = item['category']

        x = []
        for token in item['title']:
            x.append(get_word_embedding(word_vectors, token))
        for tag in item['tag']:
            x.append(get_word_embedding(word_vectors, tag))
        X.append(np.array(x))

    return X, cates, len(list_uniq_cate)


def num_conv_channel(out, n_layer=3):
    return [1024] * (n_layer - 1) + [out]


if __name__ == '__main__':
    domain = sys.argv[1]
    model_name = sys.argv[2]
    out_channels = int(sys.argv[3])
    dictionary = Word2Vec.load('assets/dictionary')
    word_vectors = dictionary.wv
    print 'Finish loading Word2Vec model! Size: %d' % len(word_vectors.vocab)
    del dictionary

    items = load_items(domain)
    cate_tree = load_cate_tree(domain)

    if model_name == 'cnn':
        X, cates, num_classes = build_dataset_cnn(items, word_vectors)
        X_train, X_test, Y_train, Y_test = train_test_split(X, cates, test_size=0.05, random_state=42)

        cnn_classify = HierarchicalTextClassifyCnnNet(embedding_size=WORD_EMBEDDING_SIZE,
                                                      sequence_length=TITLE_LIMIT + TAG_LIMIT,
                                                      tree=cate_tree, filter_sizes=FILTER_SIZES,
                                                      out_channels=out_channels)

        start_time = time.time()
        fit_cnn(cnn_classify, {
            'X_train': X_train,
            'X_test': X_test,
            'Y_train': Y_train,
            'Y_test': Y_test,
        }, save_path="report/model_{}_cnn_{}".format(domain, out_channels),
                report_path="report/report_{}_cnn_{}.csv".format(domain, out_channels))
        end_time = time.time()

        print('Total time: {:.2f}h'.format((end_time - start_time) / 3600))

    if model_name == 'tcn':
        X, cates, num_classes = build_dataset_dynamic_cnn(items, word_vectors)
        X_train, X_test, Y_train, Y_test = train_test_split(X, cates, test_size=0.05, random_state=42)

        tcn_classify = HierarchicalTextClassifyTcnNet(embedding_size=WORD_EMBEDDING_SIZE,
                                                      num_channels=num_conv_channel(out_channels, 5),
                                                      tree=cate_tree)
        start_time = time.time()
        fit_tcn(tcn_classify, {
            'X_train': X_train,
            'X_test': X_test,
            'Y_train': Y_train,
            'Y_test': Y_test,
        }, save_path="report/model_{}_tcn_{}".format(domain, out_channels),
                report_path="report/report_{}_tcn_{}.csv".format(domain, out_channels))
        end_time = time.time()

        print('Total time: {:.2f}h'.format((end_time - start_time) / 3600))
